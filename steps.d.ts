/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file.js');
type homePage = typeof import('./page-objects/home');
type MyHelper = import('./helpers/CustomHelper');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any, homePage: homePage }
  interface Methods extends Playwright, MyHelper {}
  interface I extends ReturnType<steps_file>, WithTranslation<MyHelper> {}
  namespace Translation {
    interface Actions {}
  }
}
