require('ts-node/register');
const { setHeadlessWhen } = require('@codeceptjs/configure');

setHeadlessWhen(process.env.HEADLESS == 'true');

exports.config = {
  tests: './tests/*.test.ts',
  output: './output',
  helpers: {
    Playwright: {
      url: 'https://gitlab.com',
      show: true,
      browser: 'chromium',
      waitForAction: '3000',
      chromium: {
        args: ['--auth-server-whitelist=""']
      }
    },
    MyHelper: {
      require: "./helpers/CustomHelper.ts", // path to module
    }
  },
  include: {
    I: './steps_file.js',
    homePage: './page-objects/home.ts',
  },
  bootstrap: null,
  mocha: {},
  name: 'e2e',
  plugins: {
    allure: {},
    pauseOnFail: {
      enabled: true
    },
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}