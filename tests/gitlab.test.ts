Feature("Gitlab");

Scenario("test something wrong", ({ I }) => {
  I.amOnPage("/");
  I.see("Gitlab");
});

Scenario("test something true", ({ I }) => {
  I.amOnPage("/");
  I.see("Explore GitLab");
});

Scenario("Je peux postuler chez Kanoma", async ({ I }) => {
  I.amOnPage("https://www.kanoma.fr/join-the-tribe/");
  I.see("JOIN THE TRIBE");
  I.fillField("Email", "cyril.hue@kanoma.fr");
  I.scrollTo(locate("#gform_submit_button_2"));
  I.see("Envoyer");
});

/**
 
fixture `Getting Started`
    .page `http://devexpress.github.io/testcafe/example`;

test('My first test', async t => {
    await t
        .typeText('#developer-name', 'John Smith')
        .click('#submit-button')

        // Use the assertion to check if the actual header text is equal to the expected one
        .expect(Selector('#article-header').innerText).eql('Thank you, John Smith!');
 */
Scenario.only("My first test", ({ I }) => {
  I.amOnPage("http://devexpress.github.io/testcafe/example");
  I.fillField("#developer-name", "John Smith");
  I.click("#submit-button");
  I.see("Thank you, John Smit!", locate("#article-header"));
});
